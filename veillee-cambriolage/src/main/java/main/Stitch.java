package main;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Stitch {
	
	private static final OkHttpClient client = new OkHttpClient();

	public static void run(String ip, String message) throws Exception {
		HttpUrl.Builder urlBuilder = HttpUrl.parse("http://"+ip+"/message").newBuilder();
		Request request = new Request.Builder()
				.url(urlBuilder.build().toString())
				.addHeader("Authorization", "Basic YWRtaW46cGFzc3dvcmQ=")
				.build();

		client.newCall(request).enqueue(new Callback() {
			@Override public void onFailure(Call call, IOException e) {
				e.printStackTrace();
			}

			@Override 
			public void onResponse(Call call, final Response response) throws IOException {
				
			}
		});
	}
}
