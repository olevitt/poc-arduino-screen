package main;

import java.io.IOException;
import java.util.Scanner;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Main {
	
	private static String IP = null;
	private static final String CODE_A_TROUVER = "code";

	public static void main(String[] args) {
		if (args.length > 0) {
			IP = args[0];
		}
		else {
			IP = "192.168.1.82";
		}
		while (true) {
			System.out.println("Bipbip, Stitch corrompu, Stitch besoin aide");
			System.out.print("Code : ");
			Scanner scanner = new Scanner(System.in);
			String input = scanner.nextLine();
			System.out.println("Connexion à Stitch ...");
			for (int i = 0; i < 10; i++) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int j = 0; j < (i+1)*2; j++) {
					System.out.print(".");
				}
				System.out.println();
			}
			System.out.println("Code envoyé");
			if (CODE_A_TROUVER.equals(input)) {
				sendMessageToStich("BRAVO");
				try {
					Thread.sleep(30*1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return;
			}
			else {
				sendMessageToStich(":( :( :(");
			}
			try {
				System.out.println();
				System.out.println();
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void playSound()  {
		try {
			Clip clip = AudioSystem.getClip();
			AudioInputStream inputStream = AudioSystem.getAudioInputStream(Main.class.getResource("machin.wav"));
	        clip.open(inputStream);
	        clip.start(); 
		}
        catch (Exception e) {
        	
        }
	}
	
	public static void sendMessageToStich(String message) {
		try {
			Stitch.run(IP,message);
		}
		catch (Exception e) {
			
		}
	}
}
