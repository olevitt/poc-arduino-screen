package com.example.gon.robot;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MainActivity extends AppCompatActivity {

    EditText text;
    EditText ip;
    EditText speed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         text = findViewById(R.id.texte);
         ip = findViewById(R.id.ip);
         speed = findViewById(R.id.speed);
    }

    public void envoyer(View v) {
        try {
            run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final OkHttpClient client = new OkHttpClient();

    public void run() throws Exception {
        HttpUrl.Builder urlBuilder = HttpUrl.parse("http://"+ip.getText().toString()+"/message").newBuilder();
        urlBuilder.addQueryParameter("marqueeMsg", text.getText().toString());
        urlBuilder.addQueryParameter("speed",speed.getText().toString());

        text.setText("");
        Request request = new Request.Builder()
                .url(urlBuilder.build().toString())
                .addHeader("Authorization", "Basic YWRtaW46cGFzc3dvcmQ=")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override public void onResponse(Call call, final Response response) throws IOException {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Toast.makeText(MainActivity.this,"reçu" + response.body().string(),Toast.LENGTH_SHORT).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
}
