package main;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws IOException {
		String[] phrases = new String[] {"Je suis un ducky","Coin coin","Il fait tellement chaud ici","Il fait tellement froid ici","Ou sont mes amis ?","J'ai faim","J'ai soif","Ou est la baignoire la plus proche, s'il vous plait ?","J'aimerais tellement me baigner","Zzz","Quel est le comble pour un canard ?","Pourquoi les canards sont toujours à l'heure ?"};
		Scanner scanner = new Scanner(System.in);
		Random r = new Random();
		while (true) {
			System.out.println("En attente de branchement de ducky ...");
			String input = scanner.nextLine();
			for (int i = 0; i < 50; i++) {
				for (int j = 0; j < 420; j++) {
					System.out.print((char)(r.nextInt('z' - '$') + '$'));
				}
				try {
					Thread.sleep(r.nextInt(4) * 100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println();
			}
			System.out.println();
			System.out.println(rot10(phrases[(int) (Math.random()*phrases.length)]));
		}
		
	}

	public static String rot10(String s) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if       (c >= 'a' && c <= 'p') c += 10;
			else if  (c >= 'A' && c <= 'P') c += 10;
			else if  (c >= 'p' && c <= 'z') c -= 16;
			else if  (c >= 'P' && c <= 'Z') c -= 16;
			buffer.append(c);
		}
		return buffer.toString();
	}
}
